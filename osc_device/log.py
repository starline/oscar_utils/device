#!/usr/bin/env python

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import json
import textwrap

from time     import time
from datetime import datetime


class TEXT:

    RESET = '\033[0m'

    class BG:

        RESET   = '\033[49m'

        BLACK   = '\033[40m'
        GREY    = '\33[100m'

        RED     = '\033[41m'
        GREEN   = '\033[42m'
        YELLOW  = '\033[43m'
        BLUE    = '\033[44m'
        VIOLET  = '\033[45m'
        CYAN    = '\033[46m'
        WHITE   = '\033[47m'


        LRED    = '\33[101m'
        LGREEN  = '\33[102m'
        LYELLOW = '\33[103m'
        LBLUE   = '\33[104m'
        LVIOLET = '\33[105m'
        LCYAN   = '\33[106m'
        LWHITE  = '\33[107m'

    class FG:

        RESET   = '\033[39m'

        BLACK   = '\033[30m'
        GREY    = '\33[90m'

        RED     = '\033[31m'
        GREEN   = '\033[32m'
        YELLOW  = '\033[33m'
        BLUE    = '\033[34m'
        VIOLET  = '\033[35m'
        CYAN    = '\033[36m'
        WHITE   = '\033[37m'


        LRED    = '\33[91m'
        LGREEN  = '\33[92m'
        LYELLOW = '\33[93m'
        LBLUE   = '\33[94m'
        LVIOLET = '\33[95m'
        LCYAN   = '\33[96m'
        LWHITE  = '\33[97m'

    class STYLE:

        RESET   = '\033[22m'

        BOLD     = '\33[1m'
        ITALIC   = '\33[3m'
        UNDER    = '\33[4m'


def pptime():
    dt = datetime.now()
    dt = dt.replace(year = dt.year-2000)

    # return f'{dt:%d-%m-%Y %H:%M:%S}.{dt.microsecond // 10000:02d}'

    return f'{dt:%H:%M:%S}{dt.microsecond // 10000:02d}' + f' {dt:%d-%m-%Y}'


def pprint(text      = '',
           header    = '',
           fg        = '',
           bg        = '',
           hframe    = 0,
           tspace    = 0,           # top num of spaces
           bspace    = 0,           # bottom num of spaces
           line_size = 110,
           with_time = True):

    pptext = '\n' * tspace + fg + bg + ' ' * hframe

    # header --------------------------

    full_header = ''

    if with_time or header:

        full_header = '[ '

        if with_time:
            full_header += pptime() + ' '

        full_header += header + ' ]: '

    # ---------------------------------

    pptext += full_header

    full_header_len = len(full_header)

    text_size = line_size - full_header_len - hframe

    lines = []

    for line in text.split('\n'):

        if not line or line.isspace():
            lines.append(line)

        else:
            lines.extend(textwrap.wrap(line, text_size))

    try:
        pptext += lines.pop(0)
    except:
        pass

    for line in lines:
        pptext += '\n' + ' ' * (full_header_len + hframe) + line

    # ---------------------------------

    pptext += TEXT.RESET + '\n' * bspace

    print(pptext)


# ---------------------------------------------------------

def info(text = '', header = '', line_size = 110, with_time = True):

    pprint(text, header, hframe    = 1,
                         tspace    = 1,
                         line_size = line_size,
                         with_time = with_time)


def done(text = '', header = '', line_size = 110, with_time = True):

    pprint(text, header, fg        = TEXT.FG.GREEN,
                         hframe    = 1,
                         tspace    = 1,
                         line_size = line_size,
                         with_time = with_time)


def warn(text = '', header = 'WARN', line_size = 110, with_time = True):

    pprint(text, header, fg        = TEXT.FG.YELLOW,
                         hframe    = 1,
                         tspace    = 1,
                         line_size = line_size,
                         with_time = with_time)


def err(text = '', header = 'ERR', line_size = 110, with_time = True):

    pprint(text, header, fg        = TEXT.FG.RED,
                         hframe    = 1,
                         tspace    = 1,
                         line_size = line_size,
                         with_time = with_time)
