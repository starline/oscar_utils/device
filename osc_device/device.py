#!/usr/bin/env python

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import json
import traceback

from threading import Event, Lock, Thread
from time      import sleep, time, time_ns
from uuid      import uuid4

from paho.mqtt                  import client           as mqtt
from paho.mqtt.properties       import Properties       as MqttProps
from paho.mqtt.enums            import MQTTErrorCode    as MqttErrorCode
from paho.mqtt.packettypes      import PacketTypes      as MqttTypes
from paho.mqtt.subscribeoptions import SubscribeOptions as MqttSubOpts

from . import log


class DeviceCfg(dict):

    def __init__(self, *args, **kwargs):

        self['proto']       = None

        self['broker_host'] = 'localhost'
        self['broker_port'] = 1883

        self['broker_expiry'] = 60 * 4          # session expiry in sec

        self['reconnect_timeout'] = 4

        self['uuid'] = ''
        self['ns']   = ''                                # mqtt prefix

        # TODO:
        # self['log'] = {'stdout' : ['text', 'info', 'warn', 'err'],
        #                'file'   : {},   # 'уровень' : путь/до/файла
        #                'oscd'   : {} }  # 'уровень' : ns/до/топика
        #                                 # или вместо {} путь (ns)

        # -------------------------------------------------

        super(DeviceCfg, self).__init__(*args, **kwargs)

        self['ns'] = _fix_ns(self['ns'])

        if not self['uuid']:
            self['uuid'] = 'd' + uuid4().hex[:5].upper()

        if self['proto'] == 'oscd_v0.1':
            self['ns'] = 'oscd/v0.1/' + self['uuid'].split('.')[0] + _fix_ns(self['ns'])


class Device:

    def __init__(self, cfg           = None,
                       connect_cb    = None,
                       message_cb    = None,
                       disconnect_cb = None,
                       autocon       = True):

        if isinstance(cfg, DeviceCfg):
            self._cfg = cfg

        elif isinstance(cfg, dict):
            self._cfg = DeviceCfg(cfg)

        else:
            self._cfg = DeviceCfg()

        # -----------------------------

        self._connect_cb    = connect_cb
        self._message_cb    = message_cb
        self._disconnect_cb = disconnect_cb

        # -----------------------------

        self._cl = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,
                               client_id  = self._cfg['uuid'],
                               protocol   = mqtt.MQTTv5,
                               transport  = "tcp")

        self._cl.reconnect_delay_set(1, self._cfg['reconnect_timeout'])

        self._cl.on_connect    = self._on_mqtt_connect
        self._cl.on_disconnect = self._on_mqtt_disconnect

        self._cl.on_message    = self._on_mqtt_message

        # -------------------------------------------------

        self.connected = False

        if autocon:
            self.connect()


    def connect(self):

        connect_props = MqttProps(MqttTypes.CONNECT)
        connect_props.SessionExpiryInterval = self._cfg['broker_expiry']

        try:

            #   paho-mqtt cli.connect при недоступности сервера при
            # первом подключении вываливает Exception сокета снизу:
            #
            #  - ConnectionRefusedError: [Errno 111] Connection refused
            #
            #   Но при этом потом сам переподключается при доступности
            # сервера. Зачем? А ктож его знает.

            self._cl.connect(host        = self._cfg['broker_host'],
                             port        = self._cfg['broker_port'],
                             keepalive   = 10,
                             clean_start = False,
                             properties  = connect_props)

        except:       # TODO: handle all raises types
            pass

        self._cl.loop_start()


    def is_connected(self):
        return self._cl.is_connected()


    def _on_mqtt_connect(self, client, userdata, flags, rc, props = None):

        self.connected = True

        if self._connect_cb:

            try:
                self._connect_cb()

            except:
                log.err(traceback.format_exc(), 'DVCE | e01')

        # -------------------------------------------------

        # if not flags.get('session present'):

        #     res, mid = self._cl.subscribe(("fleet/#", MqttSubOpts(qos = 2)))

        #     if res == MqttErrorCode.MQTT_ERR_NO_CONN:
        #         pass                                    # TODO: hadle NO_CONN


    def _on_mqtt_disconnect(self, client, userdata, flags, rc, props = None):

        # Если rc (reason code) == "Keep alive timeout", paho-mqtt вызывает
        # on_disconnect дважды.

        self.connected = False

        if self._disconnect_cb:

            try:
                self._disconnect_cb(rc)

            except:
                log.err(traceback.format_exc(), 'DVCE | e03')

        # print('\n Disconnected:')
        # print('   client     : ', client)
        # print('   userdata   : ', userdata)
        # print('   flags      : ', flags)
        # print('   properties : ', props)
        # print('   rc         : ', rc)           # reason_code


    # -----------------------------------------------------

    def _on_mqtt_message(self, client, userdata, msg):

        if self._message_cb:

            try:
                self._message_cb(json.loads(msg.payload.decode('utf-8')),
                                 str(msg.topic),
                                 msg.mid)

            except:
                log.err(traceback.format_exc(), 'DVCE | e02')

        # topic_ns = topic.split('/')
        # uuid, topic = message.topic.split('/', 1)


    def sub(self, topic: str):

        if not self._cl.is_connected():
            return False

        if self._cfg['ns']:
            topic = _fix_ns(topic)

        res, mid = self._cl.subscribe((self._cfg['ns'] + topic,
                                       MqttSubOpts(qos = 2)))

        return True if res == MqttErrorCode.MQTT_ERR_SUCCESS else False

        # if res == MqttErrorCode.MQTT_ERR_NO_CONN:
        #     pass


    def unsub(self, topic: str):

        if not self._cl.is_connected():
            return False

        if self._cfg['ns']:
            topic = _fix_ns(topic)

        res, mid = self._cl.unsubscribe((self._cfg['ns'] + topic,
                                         MqttSubOpts(qos = 2)))

        return True if res == MqttErrorCode.MQTT_ERR_SUCCESS else False


    def set_message_cb(self, cb):
        self._message_cb = cb


    def send(self, data, topic = '', timeout = 2):

        if not self._cl.is_connected():
            return False

        try:

            if self._cfg['proto'] == 'oscd_v0.1':

                msg = {'data' : data,
                       'head' : {'id' : self._cfg['uuid'],
                                 'ts' : time_ns()} }
            else:
                msg = data

            if self._cfg['ns']:
                topic = _fix_ns(topic)

            msg_info = self._cl.publish(topic   = self._cfg['ns'] + topic,
                                        payload = json.dumps(msg).encode('utf-8'),
                                        qos     = 2,
                                        retain  = False)

        except:       # TODO: handle all raises types
            return False

        try:
            msg_info.wait_for_publish(timeout)

        except:       # TODO: handle all raises types
            return False

        return True


# common ----------------------------------------------------------------- #

class StoppableThread(Thread):

    def __init__(self,  target = None, *args, **kwargs):

        super(StoppableThread, self).__init__(target = target,
                                              *args,
                                              **kwargs)

        self._stop_event = Event()
        self._stopped    = Event()
        self.daemon      = True

        self._stopped.set()


    def stop(self):
        self._stop_event.set()


    def stopped(self):
        return self._stopped.is_set()


    def running(self):
        return not (self._stop_event.is_set() or self._stopped.is_set())


    def run(self):

        self._stopped.clear()
        self._stop_event.clear()

        if self._target:
            self._target(*self._args, **self._kwargs)

        self._stopped.set()


    def wait_for_stop(self, timeout = None):
        return self._stopped.wait(timeout)


class Timer():

    def __init__(self, callback, period     = 1.0,
                                 miss_first = True,
                                 start      = True):

        self._target = callback
        self._period = period
        self._thread = None

        self._miss_first = miss_first

        if start:
            self.start()


    def _rated_loop(self, *args, **kwargs):

        if self._miss_first:
            sleep(self._period)

        next_call_time = time()

        while self._thread.running():

            next_call_time = next_call_time + self._period

            self._target(*args, **kwargs)

            rest_time = next_call_time - time()

            sleep(max(0, rest_time))


    def is_active(self):
        return (self._thread and self._thread.is_alive())


    def start(self,  *args, **kwargs):

        if not self.is_active():

            self._thread = StoppableThread(self._rated_loop, *args, **kwargs)
            self._thread.start()


    def stop(self):

        if self._thread:
            self._thread.stop()


    def wait_for_stop(self, timeout = None):
        self._thread.wait_for_stop(timeout)


    def stopped(self):
        return self._thread.stopped()


def spin():

    while True:

        try:
            sleep(2)

        except KeyboardInterrupt:
            break


# helpers ---------------------------------------------------------------- #

def _fix_ns(ns: str):

    if ns:

        if ns[0] != '/':
            ns = '/' + ns

        if ns[-1] == '/':
            ns = ns[:-1]

    return ns