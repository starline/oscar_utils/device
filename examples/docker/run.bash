#!/usr/bin/env bash

PKG_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

docker run  -d -ti --rm                                    \
            -v ${PKG_PATH}:/osc_device                     \
           --net=host                                      \
           --privileged                                    \
           --name osc_device_examples                      \
           osc_device_examples                             \
           > /dev/null

