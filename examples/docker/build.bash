#!/usr/bin/env bash

PKG_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

DOCKER_BUILDKIT=1                                                   \
docker build -t osc_device_examples                                 \
             -f ${PKG_PATH}/examples/docker/Dockerfile ${PKG_PATH}  \
            --network=host
