#!/usr/bin/env python

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import random
import time

from osc_device import Device, Timer, spin

import osc_device.log as log


SEND_DATA_PERIOD = 5
SEND_BUF_Q_SIZE  = 5 * 60 / SEND_DATA_PERIOD


class TempPublisher:

    def __init__(self, device):

        self._device = device

        self._aggregate_buf = []

        self._send_buf = []
        self._send_tm  = Timer(self._send_loop, SEND_DATA_PERIOD)

        # -------------------------------------------------

        self._temp_topic = 'sensor/temp'

        self._temp_data_generator_tm = Timer(self._temp_gen_loop, 1)


    def _temp_gen_loop(self):

        data = (time.time(), round(random.uniform(25.0, 70.0), 1))
        self._aggregate_buf.append(data)


    def _send_loop(self):

        if self._aggregate_buf:

            self._send_buf.append(self._aggregate_buf.copy())
            self._aggregate_buf = []

        if not self._send_buf:
            return

        # -------------------------------------------------

        combined = []

        for chunk in self._send_buf:
            combined.extend(chunk)

        # -------------------------------------------------

        if self._device.send(combined, self._temp_topic, SEND_DATA_PERIOD * 0.8):
            self._send_buf = []

        else:
            send_buf_len = len(self._send_buf)

            if send_buf_len > SEND_BUF_Q_SIZE:
                self._send_buf.pop(send_buf_len // 2)


# ------------------------------------------------------------------------ #

def connect_cb():
    info = 'Connected to broker ( ' + str(d_cfg['broker_host']) + ':'      \
                                    + str(d_cfg['broker_port']) + ' ) as ' \
                                    + str(d_cfg['uuid'])
    log.done(info, 'HOOK | d01')


def disconnect_cb(rc):
    info = 'Disconnected from broker ( '              \
                + str(d_cfg['broker_host']) + ':'     \
                + str(d_cfg['broker_port']) + ' )'    \
                + '\n\npaho-mqtt reason: ' + str(rc)

    log.warn(info, 'HOOK | w01')


# ------------------------------------------------------------------------ #

if __name__ == '__main__':

    d_cfg = {"broker_host" : "localhost",
             "broker_port" : 1883,
             "proto"       : "oscd_v0.1",
             "uuid"        : "dummy"}

    device = Device(cfg           = d_cfg,
                    connect_cb    = connect_cb,
                    disconnect_cb = disconnect_cb)

    tp = TempPublisher(device)

    spin()
