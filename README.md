### OSC Device

Модуль-обертка над *MQTT*, упрощающий сбор данных и взаимодействие с *устройствами* и беспилотными ТС, разрабатываемыми в рамках проекта [OSCAR](https://gitlab.com/starline/oscar).


#### Установка из исходников

```
git clone https://gitlab.com/starline/oscar_utils/device.git && cd device
pip3 install --user -e .
```

#### Использование и API

Модуль может использоваться как упрощенный клиент MQTT, так и с поддержкой
протокола oscd. Используется **MQTTv5**, соответственно, брокер должен
поддерживать эту версию.

Для конфигурации *устройства* используется конфиг:

```python
class DeviceCfg(dict):

    def __init__(self, *args, **kwargs):

        self['proto']       = None

        self['broker_host'] = 'localhost'
        self['broker_port'] = 1883

        self['broker_expiry'] = 60 * 4         # session expiry in sec

        self['reconnect_timeout'] = 4

        self['uuid'] = ''
        self['ns']   = ''

```

Поле `ns` подставляется в начало имени MQTT-топика перед публикацией.
Таким образом можно переиспользовать код меняя только конфиг.

Поле `uuid` используется в качестве clien_id для MQTT.


* **Примеры**

    - Публикация данных с oscd: [pub_oscd.py](./examples/pub_oscd.py)

    - В составе стека Apollo:
      [oscar / hook](https://gitlab.com/starline/oscar/-/tree/master/modules/oscar/hook)


* **OSCD**

    Протокол используется внутренними сервисами для упрощения идентификации и обработки поступающих данных и вводит следующие особенности:

    - Каждое публикуемое через `send()` сообщение приводится к следующему виду:

        ```python
        {'data' : data,                    - данные из send(data)
         'head' : {'id' : cfg['uuid'],
                   'ts' : time_ns} }       - время отправки в unixtime (нс)
        ```

    - Поле `uuid` может быть использовано для задания компоненты *устройства*,
    и имеет следующий формат: `device_uuid.path.to.component`.

        Например: `actros.logger`, `lexus.alpha.monitor`.

    - Путь публикуемых mqtt-топиков всегда начинается с `oscd/<ver>/<device_uuid>`.

        В `<device_uuid>` при этом убирается часть, связанная с компонентой (все, что после **.** ). При задании в конфиге `{'uuid' : 'actros.logger', 'proto' : 'oscd_v0.1'}` полный путь MQTT-топика будет `oscd/v0.1/actros`.

* **API**

    После установки модуль доступен под именем **osc_device**.

    - **Device** - основной класс для модуля. При создании соответствующего
    объекта все аргументы необязательны. В случае отсутствия конфига, он
    будет создан с параметрами по умолчанию.

        При задании `autocon = False`, перед началом публикации потребуется
        явно вызвать метод `connect()`.

        ```python
        class Device:

        def __init__(self, cfg: DeviceCfg | dict = None,
                           connect_cb            = None,
                           message_cb            = None,
                           disconnect_cb         = None,
                           autocon               = True):

            ...

            self.connected = False

            if autocon:
                self.connect()


        def connect(self):
            ...


        def sub(self, topic: str) -> bool:
            ...


        def unsub(self, topic: str) -> bool:
            ...


        def set_message_cb(self, cb):
            ...


        def send(self, data, topic = '', timeout = 2) -> bool:
            ...

        ```


    - **Timer** - второстепенный класс. Позволяет выполнять `callback`
        в отдельном потоке с задаваемой частотой.

        ```python
        class Timer():

            def __init__(self, callback, period     = 1.0,
                                         miss_first = True,
                                         start      = True):

                ...


            def is_active(self) -> bool:
                ...


            def start(self,  *args, **kwargs):
                ...


            def stop(self):
                ...


            def wait_for_stop(self, timeout = None) -> bool:
                ...


            def stopped(self) -> bool:
                ...

        ```


    - **spin** - метод ожидающий KeyboardInterrupt.